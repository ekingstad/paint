
import numpy as np
import pytesseract as tess
from PIL import Image as pim
import matplotlib.pyplot as plt
from pythonRLSA import rlsa
from skimage.morphology import binary_erosion, binary_dilation, disk
from scipy.ndimage import label
from scipy.ndimage.filters import gaussian_filter


src = '/Users/evankingstad/bitbucket/data/paint/paint_pngs/'
# fn = 'Shieldex Wash Primer_AC 3 (2)_1.png'
# fn = 'RR_87_1.png'
fn = 'SPF_Acr06V1_DTM PC Mull_A4.png'

path = src + fn

img = pim.open(path)

tess.image_to_string(img)


bxs = tess.image_to_boxes(img)
bxs = bxs.split('0\n')

cnvs = np.zeros((img.size[1], img.size[0]))
for k, bx in enumerate(bxs):
    bx = bx[1:-1]
    x_min, y_min, x_max, y_max = [int(s) for s in bx.split() if s.isdigit()]
    cnvs[y_min:y_max, x_min:x_max] = 255
cnvs = cnvs.astype('uint8')
cnvs = np.flip(cnvs, axis=0)

er = binary_erosion(cnvs, selem=disk(2)).astype(int)*255
dil = binary_dilation(er, selem=disk(5))
dil = (dil+1)%2

# Horizontal RLSA for small sublines
rlsah_dil_wrds = rlsa.rlsa(dil.copy(), True, False, 50)
rlsah_dil_wrds = ((rlsah_dil_wrds+1)%2)*255

# Horizontal RLSA for entire sublines
rlsah_dil_lns = rlsa.rlsa(dil.copy(), True, False, 2000)
rlsah_dil_lns = ((rlsah_dil_lns+1)%2)*255

# Label components
comps1, n_feats1 = label(rlsah_dil_wrds)
comps2, n_feats2 = label(rlsah_dil_lns)

# Group sublines to lines
lns_to_sblns = {}
for f1 in range(1, n_feats1 + 1):
    ys, xs = np.where(comps1 == f1)
    for f2 in np.unique(comps2[ys, xs]):
        if f2 not in lns_to_sblns:
            lns_to_sblns[f2] = []
        lns_to_sblns[f2].append(f1)


p = 100
lns_to_txt = {}
for f2 in range(1, n_feats2 + 1):
    print('{} of {}'.format(f2, n_feats2+1))
    lns_to_txt[f2] = []
    ys = np.where(comps2 == f2)[0]
    my, My = min(ys), max(ys) + 1
    for f1 in lns_to_sblns[f2]:
        xs = np.where(comps1 == f1)[1]
        mx, Mx = min(xs), max(xs) + 1
        imgnh = np.asarray(img.crop((mx, my, Mx, My)))
        txt = tess.image_to_string(gaussian_filter(imgnh, sigma=1))
        lns_to_txt[f2].append(txt)



#
#
# p = 100
# count = 0
# lns_to_txt = {}
# for f2 in range(1, n_feats2 + 1):
#     print('{} of {}'.format(f2, n_feats2+1))
#     lns_to_txt[f2] = []
#     ys = np.where(comps2 == f2)[0]
#     my, My = min(ys), max(ys) + 1
#     for f1 in lns_to_sblns[f2]:
#         xs = np.where(comps1 == f1)[1]
#         mx, Mx = min(xs), max(xs) + 1
#         imgnh = np.asarray(img.crop((mx, my, Mx, My)))
#         masknh = ((comps1[my:My, mx:Mx] == f1) == True).astype('uint8')
#         slc = imgnh*masknh
#         thresh = filters.threshold_otsu(slc)
#         slc = (slc > thresh).astype('uint8')*255
#         slc = cv2.copyMakeBorder(slc, top=p, bottom=p, left=p, right=p, borderType=cv2.BORDER_CONSTANT, value=0)
#         b_slc = gaussian_filter(slc, sigma=1)
#         imgnh = cv2.copyMakeBorder(imgnh, top=p, bottom=p, left=p, right=p, borderType=cv2.BORDER_CONSTANT, value=0)
#         ski.imsave(slc_dir + '{}.png'.format(count), imgnh)
#         txt = tess.image_to_string(gaussian_filter(imgnh, sigma=1))
#         lns_to_txt[f2].append(txt)
#         count += 1

