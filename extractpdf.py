#
import pytesseract as tess
from PIL import Image as pim
from wand.image import Image as wim

#
class Extractor:

    #
    def __init__(self, resolution = 300, save_ext = 'png'):
        self.rez = resolution
        self.s_ext = save_ext

    #
    def convertpdf(self, ldir, sdir, fn):
        n_path_acc = []
        path = ldir + fn
        with(wim(filename=path, resolution=self.rez)) as src:
            imgs = src.sequence
            for i in range(len(imgs)):
                n_path = '{}{}_{}.{}'.format(sdir, fn[:(-len(self.s_ext) - 1)], i + 1, self.s_ext)
                n_path_acc.append(n_path)
                wim(imgs[i]).save(filename=n_path)
        return n_path_acc

    #
    def extracttext(self, path):
        return tess.image_to_string(pim.open(path))

    #
    def process(self, ldir, sdir, txtdir, fn):
        txts = {}
        n_paths = self.convertpdf(ldir, sdir, fn)
        for k, n_path in enumerate(n_paths):
            txts[n_path] = self.extracttext(n_path)
            txt_file = open('{}{}_{}.{}'.format(txtdir, fn[:(-len(self.s_ext) - 1)], k + 1, 'txt'), 'w')
            txt_file.write(txts[n_path])
            txt_file.close()
        return txts
