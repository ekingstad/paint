
import os
import scipy.misc as ski
import cv2
import matplotlib.pyplot as plt

src = '/Users/evankingstad/bitbucket/data/paint/paint_forms/'
ext_l = 'pdf'

dst = '/Users/evankingstad/Desktop/'
ext_s = 'png'

# fn = '239648_4803'
fn = '239655_4803'

l_fn = '{}{}.{}'.format(src, fn, ext_l)
s_fn = '{}{}.{}'.format(dst, fn, ext_s)

convert_ = False
if convert_:
    os.system('sips -s format {} {} --out {}'.format(ext_s, l_fn, s_fn))

img = ski.imread(s_fn, mode='L')
plt.imshow(img)
mser = cv2.MSER_create()

#Resize the image so that MSER can work better
img = cv2.resize(img, (500, 500))

gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
vis = img.copy()

regions = mser.detectRegions(gray)
hulls = [cv2.convexHull(p.reshape(-1, 1, 2)) for p in regions[0]]
cv2.polylines(vis, hulls, 1, (0,255,0))

cv2.namedWindow('img', 0)
cv2.imshow('img', vis)
while(cv2.waitKey()!=ord('q')):
    continue
cv2.destroyAllWindows()

