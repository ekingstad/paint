
import os
import cv2
import scipy.misc as ski
# import matplotlib.pyplot as plt
from wand.image import Image

src = '/Users/evankingstad/bitbucket/data/paint/paint_forms/'
ext_l = 'pdf'

dst = '/Users/evankingstad/Desktop/'
ext_s = 'png'

# fn = '239648_4803'
# fn = '239655_4803'
# fn = 'Amirez 148024 - 25 Floor Paint - Semi -Glossy DER 353'
fn = 'K14401_0_1_'

l_fn = '{}{}.{}'.format(src, fn, ext_l)
s_fn = '{}{}.{}'.format(dst, fn, ext_s)

#
# image_pdf = Image(filename=l_fn, resolution=600)
# image_tif = image_pdf.convert('tiff')


with(Image(filename=l_fn, resolution=300)) as source:
    images = source.sequence
    pages = len(images)
    for i in range(pages):
        newfilename = '{}_{}_.{}'.format(s_fn[:(-len(ext_s) - 1)], i+1, ext_s)
        Image(images[i]).save(filename=newfilename)