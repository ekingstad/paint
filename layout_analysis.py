
# Layout analysis

from pdfminer.layout import LAParams
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import PDFPageAggregator
from pdfminer.pdfpage import PDFPage
from pdfminer.layout import LTTextBoxHorizontal

datadir = '/Users/evankingstad/bitbucket/data/paint/paint_forms/'
fn = '239648_4803.pdf'

document = open(datadir + fn, 'rb')
#Create resource manager
rsrcmgr = PDFResourceManager()
# Set parameters for analysis.
laparams = LAParams()
# Create a PDF page aggregator object.
device = PDFPageAggregator(rsrcmgr, laparams=laparams)
interpreter = PDFPageInterpreter(rsrcmgr, device)
for page in PDFPage.get_pages(document):
    interpreter.process_page(page)
layout = device.get_result()
for element in layout:
    if isinstance(element, LTTextBoxHorizontal):
        print(element.get_text())



