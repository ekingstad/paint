
import numpy as np
from extractpdf2 import Extractor
import os
import pickle

textr = Extractor()

src = '/Users/evankingstad/bitbucket/data/paint/paint_pdfs/'
dst = '/Users/evankingstad/bitbucket/data/paint/paint_pngs/'
txt_dst = '/Users/evankingstad/bitbucket/data/paint/paint_txts/'

fnames = os.listdir(src)
N = len(fnames)

pick_rnd = True
if pick_rnd:
    fn = fnames[np.random.randint(N)]
    print(fn)
    fntxt = textr.process(src, dst, txt_dst, fn)
else:
    for fn in fnames:
        fntxt = textr.process(src, dst, txt_dst, fn)
        pickle.dump(fntxt, open('{}{}.pickle'.format(txt_dst, fn[:-1]), 'wb'), protocol=pickle.HIGHEST_PROTOCOL)


