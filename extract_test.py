
import numpy as np
from extractpdf import Extractor
import os
import re
import pickle

textr = Extractor()

src = '/Users/evankingstad/bitbucket/data/paint/paint_pdfs/'
dst = '/Users/evankingstad/bitbucket/data/paint/paint_pngs/'
txt_dst = '/Users/evankingstad/bitbucket/data/paint/paint_txts/'

fnames = os.listdir(src)
N = len(fnames)

pick_rnd = True
if pick_rnd:
    fn = fnames[np.random.randint(N)]
    fntxt = textr.process(src, dst, txt_dst, fn)
else:
    for fn in fnames:
        fntxt = textr.process(src, dst, txt_dst, fn)
        pickle.dump(fntxt, open('{}{}.pickle'.format(txt_dst, fn[:-1]), 'wb'), protocol=pickle.HIGHEST_PROTOCOL)

delim = '蟒蛇'

ks, vs = list(zip(*list(fntxt.items())))
txt = vs[0].split('\n')

s = delim
txt = s.join(txt)

txt = re.sub('[\(\[].*?[\)\]]', '', txt)
txt = txt.lower()

txt = txt.split(delim)

badchs = {
    ''


}

# Can't remove periods for the floats. If there is no float present, then no chemical present.
# Replace hyphens
# Do a float search and remove before further string modification

for ln in txt:
    flts = re.findall('[+-]?\d+\.\d+', ln)
    if not flts:
        continue
    else:
        flt = flts[-1]
    amt = float(flt)
    ln = ln[:-len(flt)]





