
import numpy as np
import pandas as pd
pd.options.display.max_rows = 999
pd.options.display.max_columns = 999

datadir = '/Users/evankingstad/bitbucket/data/paint/'
fn = 'Raw_Materials_Sample_Request_Available.csv'

chems = pd.read_csv(datadir + fn)
list(chems.columns.values)

chemsarr = chems['Name'].to_numpy()

chemsli = list(chemsarr)

s = '@'

s = s.join(chemsli)

print(len(set(s)))

s = s.lower()

print(len(set(s)))

badchs = {'®':'',
          '™':'',
          ' ':'',
          '-':'',
          '/':'',
          '(':'',
          ')':'',
          '[':'',
          ']':'',
          ':':'',
          '.':'',
          '*':'',
          '#':'',
          ',':'',
          ', ':'',
          '&':'',
          '%':''}

for oc, nc in list(badchs.items()):
    s = s.replace(oc, nc)

weirdu = 'ü'
s = s.replace(weirdu, 'u')

print(len(set(s)))

s = s.split('@')

s = np.array(s)
s = np.expand_dims(s, axis=-1)

print(s[:1000])

