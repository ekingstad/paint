


src = '/Users/evankingstad/bitbucket/data/paint/paint_forms/'
ext_l = 'pdf'

dst = '/Users/evankingstad/Desktop/'
ext_s = 'png'

# fn = '239648_4803'
fn = '239655_4803'


l_fn = '{}{}.{}'.format(src, fn, ext_l)
s_fn = '{}{}.{}'.format(dst, fn, ext_s)



from wand.image import Image
# Converting first page into JPG
with Image(filename=l_fn) as img:
     img.save(filename=s_fn)
# Resizing this image
with Image(filename="/temp.jpg") as img:
     img.resize(200, 150)
     img.save(filename="/thumbnail_resize.jpg")