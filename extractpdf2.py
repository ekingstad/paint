


import numpy as np
import pickle
import pytesseract as tess
from PIL import Image as pim
from wand.image import Image as wim
from pythonRLSA import rlsa
from skimage.morphology import binary_erosion, binary_dilation, disk
from scipy.ndimage import label
from scipy.ndimage.filters import gaussian_filter

#
class Extractor:

    #
    def __init__(self, resolution = 300, save_ext = 'png', rlsaw1 = 50, rlsaw2 = 2000):
        self.rez = resolution
        self.s_ext = save_ext
        self.rlsaw1 = rlsaw1
        self.rlsaw2 = rlsaw2

    #
    def convertpdf(self, ldir, sdir, fn):
        n_path_acc = []
        path = ldir + fn
        with(wim(filename=path, resolution=self.rez)) as src:
            imgs = src.sequence
            for i in range(len(imgs)):
                n_path = '{}{}_{}.{}'.format(sdir, fn[:(-len(self.s_ext) - 1)], i + 1, self.s_ext)
                n_path_acc.append(n_path)
                wim(imgs[i]).save(filename=n_path)
        return n_path_acc

    #
    def extracttext(self, path):

        img = pim.open(path)
        bxs = tess.image_to_boxes(img)
        bxs = bxs.split('0\n')

        cnvs = np.zeros((img.size[1], img.size[0]))
        for k, bx in enumerate(bxs):
            bx = bx[1:-1]
            x_min, y_min, x_max, y_max = [int(s) for s in bx.split() if s.isdigit()]
            cnvs[y_min:y_max, x_min:x_max] = 255
        cnvs = cnvs.astype('uint8')
        cnvs = np.flip(cnvs, axis=0)

        er = binary_erosion(cnvs, selem=disk(2)).astype(int) * 255
        dil = binary_dilation(er, selem=disk(5))
        dil = (dil + 1) % 2

        # Horizontal RLSA for small sublines
        rlsah_dil_wrds = rlsa.rlsa(dil.copy(), True, False, self.rlsaw1)
        rlsah_dil_wrds = ((rlsah_dil_wrds + 1) % 2) * 255

        # Horizontal RLSA for entire sublines
        rlsah_dil_lns = rlsa.rlsa(dil.copy(), True, False, self.rlsaw2)
        rlsah_dil_lns = ((rlsah_dil_lns + 1) % 2) * 255

        # Label components
        comps1, n_feats1 = label(rlsah_dil_wrds)
        comps2, n_feats2 = label(rlsah_dil_lns)

        # Group sublines to lines
        lns_to_sblns = {}
        for f1 in range(1, n_feats1 + 1):
            ys, xs = np.where(comps1 == f1)
            for f2 in np.unique(comps2[ys, xs]):
                if f2 not in lns_to_sblns:
                    lns_to_sblns[f2] = []
                lns_to_sblns[f2].append(f1)

        lns_to_txt = {}
        for f2 in range(1, n_feats2 + 1):
            lns_to_txt[f2] = []
            ys = np.where(comps2 == f2)[0]
            my, My = min(ys), max(ys) + 1
            for f1 in lns_to_sblns[f2]:
                xs = np.where(comps1 == f1)[1]
                mx, Mx = min(xs), max(xs) + 1
                slc = np.asarray(img.crop((mx, my, Mx, My)))
                txt = tess.image_to_string(gaussian_filter(slc, sigma=1))
                lns_to_txt[f2].append(txt)

        return lns_to_txt


    #
    def process(self, ldir, sdir, txtdir, fn):
        txts = {}
        n_paths = self.convertpdf(ldir, sdir, fn)
        for k, n_path in enumerate(n_paths):
            txts[n_path] = self.extracttext(n_path)
            pickle.dump(txts[n_path], open('{}{}.pickle'.format(txtdir, fn[:-1]), 'wb'), protocol=pickle.HIGHEST_PROTOCOL)
        return txts